var exec = require('cordova/exec');

exports.setVolumeStream = function(arg0, success, error) {
    exec(success, error, "audiomgr", "setVolumeStream", [arg0]);
};
