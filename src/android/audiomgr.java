// featureのparamのvalueで指定したやつ
package plugin.audiomgr;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;
import android.media.AudioManager;

public class audiomgr extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {
        if (action.equals("setVolumeStream")) {
          cordova.getActivity().setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
          String catchMsg = data.getString(0);
          String message = "catchMsg: " + catchMsg;
          callbackContext.success(message);
            return true;
        } else {
            return false;
        }
    }
}
